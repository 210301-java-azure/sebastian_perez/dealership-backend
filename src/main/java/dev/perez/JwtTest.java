package dev.perez;

import dev.perez.models.User;
import dev.perez.util.JwtUtil;

public class JwtTest {

    public static void main(String[] args){
        JwtUtil jwtutil = new JwtUtil();
        User user = new User("admin", "admin_role");
        String token = jwtutil.generateToken(user);
        System.out.println(token);
        System.out.println(jwtutil.extractUsername(token));
    }

}
