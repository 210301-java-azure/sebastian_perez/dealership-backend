package dev.perez.services;

import dev.perez.data.VehicleDao;
import dev.perez.data.VehicleDaoHibImpl;
import dev.perez.models.Vehicle;

import java.util.List;

public class VehicleService {

    private VehicleDao vehicleDao = new VehicleDaoHibImpl();

    public List<Vehicle> getAll(){
        return vehicleDao.getAllVehicles();
    }

    public Vehicle getById(int id){
        return vehicleDao.getVehicleById(id);
    }

    public Vehicle add(Vehicle v){
        return vehicleDao.addNewVehicle(v);
    }

    public void delete(int id){
        vehicleDao.deleteVehicle(id);
    }

    public Vehicle update(Vehicle v){
        return vehicleDao.updateVehicle(v);
    }

    public List<Vehicle> getPriceRange(String minPrice, String maxPrice) {

        if (maxPrice != null) {
            // max provided
            // parse string input to doubles
            double max = Double.parseDouble(maxPrice)+0.1;

            if (minPrice != null) {
                //if both are provided
                // parse string input to doubles
                double min = Double.parseDouble(minPrice);
                return vehicleDao.getVehiclesInPriceRange(min, max);
            }
            // only max provided
            return vehicleDao.getVehiclesWithMaxPrice(max);
        } else {
            // only min provided
            // parse string input to doubles
            int min = Integer.parseInt(minPrice);
            return vehicleDao.getVehiclesWithMinPrice(min);
        }
    }
}

