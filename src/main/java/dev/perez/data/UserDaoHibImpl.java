package dev.perez.data;

import dev.perez.models.User;
import dev.perez.models.Vehicle;
import dev.perez.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class UserDaoHibImpl implements UserDao {

    private Logger logger = LoggerFactory.getLogger(UserDaoHibImpl.class);

    @Override
    public User authenticateUser(String username, String password) {
        try (Session s = HibernateUtil.getSession()) {
            CriteriaBuilder criteriaBuilder = s.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
            Root<User> root = criteriaQuery.from(User.class);

            Predicate predicateForUsername
                    = criteriaBuilder.equal(root.get("username"), username);
            Predicate predicateForPassword
                    = criteriaBuilder.equal(root.get("password"), password);
            Predicate predicateForCredentials
                    = criteriaBuilder.and(predicateForUsername, predicateForPassword);


            criteriaQuery.where(predicateForCredentials);
//            Query<User> query = s.createQuery(criteriaQuery);
//            User user = entityManager.createQuery(criteriaQuery).getResultList();
            User user = s.createQuery(criteriaQuery).getSingleResult();

            logger.info("UserDaoImpl ", user.getUsername(), " role: ",
                    user.getRole());
            return user;
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session s = HibernateUtil.getSession()) {

            CriteriaBuilder criteriaBuilder = s.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
            Root<User> root = criteriaQuery.from(User.class);


            criteriaQuery.where(criteriaBuilder.equal(root.get("username"), username));
//            Query<User> query = s.createQuery(criteriaQuery);
//            User user = entityManager.createQuery(criteriaQuery).getResultList();
            User user = s.createQuery(criteriaQuery).getSingleResult();

            logger.info("UserDaoImpl ", user.getUsername(), " role: ",
                    user.getRole());
            return user;
        }

    }

    @Override
    public User addNewUser(User user) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//            s.persist(vehicle);
            int id = (int) s.save(user);
            user.setId(id);
            logger.info("added new item with id:"+id);
            tx.commit();
            return user;
        }
    }
}
