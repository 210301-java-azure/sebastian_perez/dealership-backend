package dev.perez.data;

import dev.perez.models.Location;

import java.util.List;

public interface LocationDao {

    public List<Location> getAllLocations();
    public Location createLocation(Location newLocation);

}
