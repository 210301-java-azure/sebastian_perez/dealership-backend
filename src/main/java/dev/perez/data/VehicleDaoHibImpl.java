package dev.perez.data;

import dev.perez.models.Vehicle;
import dev.perez.util.HibernateUtil;
import org.hibernate.Session;

import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;


public class VehicleDaoHibImpl implements VehicleDao {

    private Logger logger = LoggerFactory.getLogger(VehicleDaoHibImpl.class);
    @Override
    public List<Vehicle> getAllVehicles() {
        try(Session s = HibernateUtil.getSession()){
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from market_item'

            List<Vehicle> vehicles = s.createQuery("from Vehicle", Vehicle.class).list();

            return vehicles;
        }

    }

    @Override
    public Vehicle getVehicleById(int id) {
        try(Session s = HibernateUtil.getSession()){
//            Vehicle v = s.load(Vehicle.class,id);
            Vehicle vehicle = s.get(Vehicle.class, id);
            return vehicle;
        }

    }

    @Override
    public Vehicle addNewVehicle(Vehicle vehicle) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//            s.persist(vehicle);
            int id = (int) s.save(vehicle);
            vehicle.setId(id);
            logger.info("added new item with id:"+id);
            tx.commit();
            return vehicle;
        }

    }

    @Override
    public void deleteVehicle(int id) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(new Vehicle(id));
            tx.commit();
        }

    }

    @Override
    public Vehicle updateVehicle(Vehicle newVehicle) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.update(newVehicle);
            tx.commit();
            return newVehicle;
        }
    }

    @Override
    public List<Vehicle> getVehiclesInPriceRange(double min, double max) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Vehicle> cq = cb.createQuery(Vehicle.class);

            Root<Vehicle> root = cq.from(Vehicle.class);
            cq.select(root);

            Predicate predicateForUsername
                    = cb.lessThanOrEqualTo(root.get("price"), max);
            Predicate predicateForPassword
                    = cb.greaterThanOrEqualTo(root.get("price"), min);
            Predicate predicateForVehiclesInRange
                    = cb.and(predicateForUsername, predicateForPassword);


            cq.where(predicateForVehiclesInRange);

            Query<Vehicle> query = s.createQuery(cq);
            return query.list();
        }

    }

    @Override
    public List<Vehicle> getVehiclesWithMaxPrice(double max) {
        logger.info("VehicleHibernate");
        try(Session s = HibernateUtil.getSession()){
            Query<Vehicle> itemQuery = s.createQuery("from Vehicle where price <= :max", Vehicle.class);
//            s.createNamedQuery("myQuery");
            itemQuery.setParameter("max",max);

                for (int i = 0; i < itemQuery.list().size() ; i++) {
                    logger.info(itemQuery.list().get(i).getMake());
                }


            return itemQuery.list();
        }

    }

    @Override
    public List<Vehicle> getVehiclesWithMinPrice(double min) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Vehicle> cq = cb.createQuery(Vehicle.class);

            Root<Vehicle> root = cq.from(Vehicle.class);
            cq.select(root);


            cq.where(cb.greaterThanOrEqualTo(root.get("price"), min));

            Query<Vehicle> query = s.createQuery(cq);
            return query.list();
        }

    }
}
