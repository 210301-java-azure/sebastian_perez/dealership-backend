package dev.perez.util;


import dev.perez.models.Location;
import dev.perez.models.User;
import dev.perez.models.Vehicle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory(){
        if(sessionFactory==null){
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
            settings.put(Environment.URL, "jdbc:sqlserver://jperez-java-azure.database.windows.net:1433;databaseName=dealership-db");
            settings.put(Environment.USER, "jperez");
            settings.put(Environment.PASS, "Ww7725230");

            //https://docs.microsoft.com/en-us/sql/connect/jdbc/working-with-a-connection?view=sql-server-ver15
            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //https://www.javatpoint.com/dialects-in-hibernate
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "update"); //create here created our tables
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            //provide hibernate mappings to configuration
            configuration.addAnnotatedClass(Vehicle.class);
            configuration.addAnnotatedClass(Location.class);
            configuration.addAnnotatedClass(User.class);

            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession(){
        return getSessionFactory().openSession();
    }

}
