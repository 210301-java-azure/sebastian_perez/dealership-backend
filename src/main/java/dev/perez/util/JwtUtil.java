package dev.perez.util;

import dev.perez.models.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


public class JwtUtil {


    private Logger logger = LoggerFactory.getLogger(JwtUtil.class);

    private  String SECRET_KEY = "secret";

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }


    public String generateToken(User userDetails) {
        Map<String, Object> claims = new HashMap<>();
//        if a user has multiple roles
//        Collection<? extends GrantedAuthority> roles = userDetails.getAuthorities();

        if(userDetails.getRole().equals("Admin_Role"))
            claims.put("isAdmin", true);
        if(userDetails.getRole().equals("User_Role"))
            claims.put("isAdmin", false);

        return createToken(claims, userDetails.getUsername());
    }



    private String createToken(Map<String, Object> claims, String subject) {
//        testtime
//        Date expDate = new Date(System.currentTimeMillis() + 1000 * 20);
//        logger.info(expDate.toString());
//        ( 1000 miliseg , 60 seg, 60 min , 10 hour)
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    //extractusername
    public Boolean validateToken(String token, User userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}